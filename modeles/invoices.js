var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var invoicesmodeles = new Schema({
    nomproduit: {type:String},
    quantite: {type:Number},
    date: {type:Date},
    total: {type:Number},
})
module.exports = mongoose.model('invoices', invoicesmodeles)